package com.demo.test.redis_demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfig {

	// Redis
	@Value("${spring.redis.master.port}")
	private String redisPort;
	@Value("${spring.redis.master.host}")
	private String redisHost;
	@Value("${spring.redis.password}")
	private String redisPassword;
	@Value("${spring.redis.timeout}")
	private String redisTimeout;
	public String getRedisPort() {
		return redisPort;
	}
	public String getRedisHost() {
		return redisHost;
	}
	public String getRedisPassword() {
		return redisPassword;
	}
	public String getRedisTimeout() {
		return redisTimeout;
	}
}
