package com.demo.test.redis_demo.config;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStaticMasterReplicaConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.lettuce.core.ClientOptions;
import io.lettuce.core.ReadFrom;
import io.lettuce.core.SocketOptions;

/**
 * {@link RedisAutoConfiguration}
 * {@link org.springframework.boot.autoconfigure.data.redis.JedisConnectionConfiguration}
 */
@Configuration
@EnableCaching
@EnableConfigurationProperties(CacheProperties.class)
public class RedisConfig {

	public static final String DEFAULT_REDIS_TEMPLATE_BEAN_NAME = "redisTemplate";
	public static final String REDIS_TEMPLATE_MANAGEMENT_BEAN_NAME = "redisTemplateManagementBean";
	public static final String LETTUCE_CONNECTION_MANAGEMENT_BEAN_NAME = "lettuceConnectionFactoryManagementBean";

	public static final String REDIS_KEY_GENERATOR_BEAN_NAME = "redisCacheKeyGenerator";

	public static final String KEY_SEPARATOR = ".";
	public static final String KEY_DIRECTORY_SEPARATOR = ":";

	@Autowired
	private RedisProperties redisProperties;
	@Autowired
	private ApplicationConfig applicationConfig;
	
	private LettuceClientConfiguration prepareLettuceClientConfiguration(Duration connectTimeout, Duration readTimeout) {
		SocketOptions soOpt = SocketOptions.builder() //
				.connectTimeout(connectTimeout) // must be greater than 0
				.build(); //
		
		ClientOptions lettuceClientOptions = ClientOptions.builder() //
				.socketOptions(soOpt) //
				.build(); //

		LettuceClientConfiguration clientConfiguration = LettuceClientConfiguration.builder() //
				.readFrom(ReadFrom.REPLICA_PREFERRED) //
				.commandTimeout(readTimeout) //
				.clientOptions(lettuceClientOptions) //
				.build(); //
		return clientConfiguration;
	}
	
	private RedisStaticMasterReplicaConfiguration prepareRedisStaticMasterReplicaConfiguration() {
		RedisStaticMasterReplicaConfiguration staticMasterReplicaConfiguration = new RedisStaticMasterReplicaConfiguration( //
				applicationConfig.getRedisHost(), //
				Integer.parseInt(applicationConfig.getRedisPort())); //
		staticMasterReplicaConfiguration.setPassword(RedisPassword.of(applicationConfig.getRedisPassword()));
		return staticMasterReplicaConfiguration;
	}
	
	@Bean
	@Primary
	public LettuceConnectionFactory redisConnectionFactoryForAPI() {
		Duration connectTimeout = redisProperties.getTimeout();
		Duration readTimeout = redisProperties.getTimeout();
		
		LettuceClientConfiguration clientConfiguration = prepareLettuceClientConfiguration(connectTimeout, readTimeout);
		RedisStaticMasterReplicaConfiguration staticMasterReplicaConfiguration = prepareRedisStaticMasterReplicaConfiguration();
		return new LettuceConnectionFactory(staticMasterReplicaConfiguration, clientConfiguration);
	}

	@Bean(LETTUCE_CONNECTION_MANAGEMENT_BEAN_NAME)
	public LettuceConnectionFactory redisConnectionFactoryForManagementTask() {
		Duration connectTimeout = Duration.ofMillis(Long.parseLong("1000"));
		Duration readTimeout = Duration.ofMillis(Long.parseLong("1000"));
		
		LettuceClientConfiguration clientConfiguration = prepareLettuceClientConfiguration(connectTimeout, readTimeout);
		RedisStaticMasterReplicaConfiguration staticMasterReplicaConfiguration = prepareRedisStaticMasterReplicaConfiguration();
		return new LettuceConnectionFactory(staticMasterReplicaConfiguration, clientConfiguration);
	}

	/**
	 * Copy from {@link org.springframework.boot.autoconfigure.cache.RedisCacheConfiguration}
	 *
	 * @param  objectMapper
	 * @return
	 */

}
