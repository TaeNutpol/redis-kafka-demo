package com.demo.test.redis_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class RedisAsaCacheWithSpringBootApplication {

   public static void main(String[] args) {
      SpringApplication.run(RedisAsaCacheWithSpringBootApplication.class, args);
   }
}