package com.demo.test.redis_demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.demo.test.redis_demo.model.security.UserCredentials;

import io.swagger.annotations.ApiOperation;

@RestController
public class LoginController {

	@ApiOperation("Login")
	@PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> savePerson( //
			@RequestHeader HttpHeaders headers, //
			@RequestBody UserCredentials request, //
			HttpServletRequest servletRequest) throws Exception { //
		System.out.println("pass");
		return ResponseEntity //
				.status(HttpStatus.OK) //
				.headers(new HttpHeaders()) //
				.body("LOGIN SUCCESS");
	}
}
