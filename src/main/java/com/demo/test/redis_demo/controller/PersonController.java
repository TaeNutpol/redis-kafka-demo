package com.demo.test.redis_demo.controller;

import java.math.BigInteger;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.test.redis_demo.db.entity.PersonEntity;
import com.demo.test.redis_demo.db.service.PersonService;
import com.demo.test.redis_demo.helper.Producer;
import com.demo.test.redis_demo.model.PersonRequestModel;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/person")
public class PersonController {
	
	@Autowired
    PersonService personService;
	
	@Autowired
	private Producer producer;

    @ApiOperation("Save Person")
	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> savePerson( //
			@RequestHeader HttpHeaders headers, //
			@RequestBody PersonRequestModel request, //
			HttpServletRequest servletRequest) throws Exception { //

		PersonEntity person = new PersonEntity();
		person.setName(request.getName());
		person.setAge(new BigInteger(request.getAge()));
		person.setSalary(new BigInteger(request.getSalary()));
		PersonEntity res = personService.savePerson(person);
		return ResponseEntity //
				.status(HttpStatus.OK) //
				.headers(new HttpHeaders()) //
				.body(res);
	}
    
    @ApiOperation("get all Person")
	@PostMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAll( //
			@RequestHeader HttpHeaders headers, //
			@RequestBody PersonRequestModel request, //
			HttpServletRequest servletRequest) throws Exception { //

		List<PersonEntity> res = personService.getAllPerson();
		return ResponseEntity //
				.status(HttpStatus.OK) //
				.headers(new HttpHeaders()) //
				.body(res);
	}

    @ApiOperation("get Person")
	@PostMapping(value = "/getById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getOne( //
			@RequestHeader HttpHeaders headers, //
			@RequestBody PersonRequestModel request, //
			HttpServletRequest servletRequest) throws Exception { //

		PersonEntity res = personService.getOnePerson(new BigInteger(request.getId()));
		return ResponseEntity //
				.status(HttpStatus.OK) //
				.headers(new HttpHeaders()) //
				.body(res);
	}
    
    @ApiOperation("update Person")
	@PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> update( //
			@RequestHeader HttpHeaders headers, //
			@RequestBody PersonRequestModel request, //
			HttpServletRequest servletRequest) throws Exception { //

    	PersonEntity person = new PersonEntity();
    	person.setName(request.getName());
		person.setAge(new BigInteger(request.getAge()));
		person.setSalary(new BigInteger(request.getSalary()));
		PersonEntity res = personService.updatePerson(person, new BigInteger(request.getId()));
		return ResponseEntity //
				.status(HttpStatus.OK) //
				.headers(new HttpHeaders()) //
				.body(res);
	}

    @ApiOperation("delete Person")
	@PostMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete( //
			@RequestHeader HttpHeaders headers, //
			@RequestBody PersonRequestModel request, //
			HttpServletRequest servletRequest) throws Exception { //

		personService.deletePerson(new BigInteger(request.getId()));
		return ResponseEntity //
				.status(HttpStatus.OK) //
				.headers(new HttpHeaders()) //
				.body("Employee with id: "+request.getId()+ " Deleted !");
	}
    
    @ApiOperation("try kafka")
	@PostMapping(value = "/kafka", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> kafka( //
			@RequestHeader HttpHeaders headers, //
			@RequestBody PersonRequestModel request, //
			HttpServletRequest servletRequest) throws Exception { //

		producer.produce(request.getName());
		return ResponseEntity //
				.status(HttpStatus.OK) //
				.headers(new HttpHeaders()) //
				.body("kafka produced");
	}
}
