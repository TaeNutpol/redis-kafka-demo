package com.demo.test.redis_demo.model.security;

public class UserCredentialsResponse {
	
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
