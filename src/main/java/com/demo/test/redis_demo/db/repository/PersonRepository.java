package com.demo.test.redis_demo.db.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.test.redis_demo.db.entity.PersonEntity;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, BigInteger> {

}
