package com.demo.test.redis_demo.db.service.impl;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.demo.test.redis_demo.db.entity.PersonEntity;
import com.demo.test.redis_demo.db.repository.PersonRepository;
import com.demo.test.redis_demo.db.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService{

	@Autowired
	PersonRepository personRepo;
	
	@Override
	public PersonEntity savePerson(PersonEntity person) {
		return personRepo.save(person);
	}
	
	@Override
	@CachePut(value="Person", key="#id")
	public PersonEntity updatePerson(PersonEntity person, BigInteger id) {
		PersonEntity invoice = personRepo.findById(id)
	            .orElse(null);
        invoice.setName(person.getName());
        invoice.setAge(person.getAge());
        invoice.setSalary(person.getSalary());
        return personRepo.save(invoice);
	}
	
	@Override
	@CacheEvict(value="Person", key="#id")
	// @CacheEvict(value="Invoice", allEntries=true) //in case there are multiple records to delete
	public void deletePerson( BigInteger id) {
		PersonEntity invoice = personRepo.findById(id)
			     .orElse(null);
		personRepo.delete(invoice);
	}
	
	@Override
	@Cacheable(value="Person", key="#id")
	public PersonEntity getOnePerson(BigInteger id) {
		PersonEntity invoice = personRepo.findById(id)
	     .orElse(null);
		return invoice;
	}
	
	@Override
	@Cacheable(value="Person")
	public List<PersonEntity> getAllPerson() {
		return personRepo.findAll();
	}
}
