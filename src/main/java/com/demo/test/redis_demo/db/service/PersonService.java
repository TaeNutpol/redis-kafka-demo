package com.demo.test.redis_demo.db.service;

import java.math.BigInteger;
import java.util.List;

import com.demo.test.redis_demo.db.entity.PersonEntity;

public interface PersonService {

	PersonEntity savePerson(PersonEntity person);

	PersonEntity updatePerson(PersonEntity person, BigInteger id);

	void deletePerson(BigInteger id);

	PersonEntity getOnePerson(BigInteger id);

	List<PersonEntity> getAllPerson();

}
